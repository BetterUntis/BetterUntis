﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BetterUntis.Untis;
using BetterUntis.Untis.Models;
using BetterUntis.Views.DetailView;
using Newtonsoft.Json;
using BetterUntis.Extensions;
using WebUntisAPI.Models;

namespace BetterUntis.Model
{
    public sealed class BetterUntisUser : IDisposable
    {
        [JsonProperty]
        private CacheElement<Teacher[]> _teachersCache = new CacheElement<Teacher[]>();
        [JsonProperty]
        private CacheElement<Student[]> _studentsCache = new CacheElement<Student[]>();
        [JsonProperty]
        private CacheElement<Class[]> _classsCache = new CacheElement<Class[]>();
        [JsonProperty]
        private CacheElement<Subject[]> _subjectsCache = new CacheElement<Subject[]>();
        [JsonProperty]
        private CacheElement<Room[]> _roomsCache = new CacheElement<Room[]>();
        [JsonProperty]
        private CacheElement<Department[]> _departmentsCache = new CacheElement<Department[]>();
        [JsonProperty]
        private CacheElement<Holiday[]> _holidaysCache = new CacheElement<Holiday[]>();
        [JsonProperty]
        private CacheElement<Day[]> _daysCache = new CacheElement<Day[]>();
        [JsonProperty]
        private CacheElement<SchoolYear> _schoolyearCache = new CacheElement<SchoolYear>();
        [JsonProperty]
        private CacheElement<SchoolYear[]> _schoolyearsCache = new CacheElement<SchoolYear[]>();
        [JsonProperty]
        private CacheElement<Period[]> _favoritePeriodsCache = new CacheElement<Period[]>();

        [JsonProperty]
        private UntisCachePolicy _cachePolicy = UntisCachePolicy.RecentCache;
        [JsonProperty]
        private int _cacheTimeInSeconds = SettingsPage.OneHour;

        [JsonConstructor]
        public BetterUntisUser(UntisUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            User = user;
        }

        public UntisUser User { get; }

        public string DisplayName { get; set; }

        [JsonProperty]
        public bool ShowMonday { get; set; } = true;
        [JsonProperty]
        public bool ShowTuesday { get; set; } = true;
        [JsonProperty]
        public bool ShowWednesday { get; set; } = true;
        [JsonProperty]
        public bool ShowThursday { get; set; } = true;
        [JsonProperty]
        public bool ShowFriday { get; set; } = true;
        [JsonProperty]
        public bool ShowSaturday { get; set; } = false;
        [JsonProperty]
        public bool ShowSunday { get; set; } = false;

        public SubjectCollection FavoriteSubjects { get; private set; } = new SubjectCollection();


        public UntisCachePolicy CachePolicy
        {
            get => _cachePolicy;
            set
            {
                if (value < UntisCachePolicy.OfflineOnly || value > UntisCachePolicy.AlwaysRecent)
                    throw new ArgumentOutOfRangeException(nameof(value));

                _cachePolicy = value;
            }
        }

        public int CacheTimeInSeconds
        {
            get => _cacheTimeInSeconds;
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value));

                _cacheTimeInSeconds = value;
            }
        }


        public Task<Teacher[]> GetTeachers()
        {
            return GetElement(_teachersCache, User.GetTeachers);
        }

        public Task<Student[]> GetStudents()
        {
            return GetElement(_studentsCache, User.GetStudents);
        }

        public Task<Class[]> GetClasses()
        {
            return GetElement(_classsCache, User.GetClasses);
        }

        public Task<Subject[]> GetSubjects()
        {
            return GetElement(_subjectsCache, User.GetSubjects);
        }

        public Task<Room[]> GetRooms()
        {
            return GetElement(_roomsCache, User.GetRooms);
        }

        public Task<Department[]> GetDepartments()
        {
            return GetElement(_departmentsCache, User.GetDepartments);
        }

        public Task<Holiday[]> GetHolidays()
        {
            return GetElement(_holidaysCache, User.GetHolidays);
        }

        public Task<Day[]> GetTimegrid()
        {
            return GetElement(_daysCache, User.GetTimegrid);
        }

        public Task<SchoolYear> GetCurrentSchoolYear()
        {
            return GetElement(_schoolyearCache, User.GetCurrentSchoolYear);
        }

        public Task<SchoolYear[]> GetSchoolYears()
        {
            return GetElement(_schoolyearsCache, User.GetSchoolYears);
        }

        public async Task<Period[]> GetTimetable(UntisElement element, DateTime startDate, DateTime endDate)
        {
            if (_cachePolicy == UntisCachePolicy.OfflineOnly)
                throw new CacheException("Offline cache is empty.");

            if (!User.LoggedIn)
                await User.Login();
            
            try
            {
                return await User.GetTimetable(element, startDate, endDate).ConfigureAwait(false);
            }
            catch (JsonRpcException ex) when (ex.Code == JsonRpcException.NotAuthenticated)
            {
                await User.Login();
                return await User.GetTimetable(element, startDate, endDate).ConfigureAwait(false);
            }
        }

        public Task<Period[]> GetFavoriteTimetable(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
                throw new ArgumentOutOfRangeException(nameof(endDate));

            var now = DateTime.Now;
            var start = now.Date.StartOfWeek(DayOfWeek.Monday);
            var end = start.AddDays(14 - 1);
            
            if (startDate >= start && endDate <= end)
                return GetElement(_favoritePeriodsCache, async () => await FavoriteSubjects.GetTimetable(this, start, end).ConfigureAwait(false));

            if (_cachePolicy == UntisCachePolicy.OfflineOnly)
                throw new CacheException("Offline cache is empty.");

            return FavoriteSubjects.GetTimetable(this, startDate, endDate);
        }

        public void InvalidateFavoriteSubjects()
        {
            _favoritePeriodsCache.Expiration = DateTime.UtcNow;
        }

        public void Dispose()
        {
            User.Dispose();
        }

        private async Task<T> GetElement<T>(CacheElement<T> entry, Func<Task<T>> func) where T : class
        {
            switch (_cachePolicy)
            {
                case UntisCachePolicy.OfflineOnly:
                    if (entry.Elemenet != null)
                        return entry.Elemenet;

                    throw new CacheException("Offline cache is empty.");
                case UntisCachePolicy.RecentCache:
                    if (entry.Elemenet != null && DateTime.UtcNow <= entry.Expiration)
                        return entry.Elemenet;

                    break;
            }

            if (!User.LoggedIn)
                await User.Login();

            T result;
            try
            {
                result = await func().ConfigureAwait(false);
            }
            catch (JsonRpcException ex) when (ex.Code == JsonRpcException.NotAuthenticated)
            {
                await User.Login();
                result = await func().ConfigureAwait(false);
            }
            
            entry.Elemenet = result;
            entry.Expiration = DateTime.UtcNow.AddSeconds(_cacheTimeInSeconds);

            return result;
        }

        private class CacheElement<T>
        {
            public T Elemenet;
            public DateTime Expiration;
        }
    }
}