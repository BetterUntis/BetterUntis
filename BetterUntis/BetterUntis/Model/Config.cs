﻿using BetterUntis.IO;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace BetterUntis.Model
{
    public class Config
    {
        private const string ConfigFile = "users.json";

        #region Seb hacks
        /// <summary>
        /// If not all rooms have a building it is
        /// -more or less efficient- to guess the buildingname
        /// </summary>
        [JsonProperty]
        public bool GuessBuildings = false;

        #endregion

        #region Colors
        [JsonIgnore]
        public Color DetailPageTextColor;
        [JsonIgnore]
        public Color DetailPageBackgroundColor;

        [JsonIgnore]
        public Color MasterPageTextColor;
        [JsonIgnore]
        public Color MasterPageBackgroundColor;

        [JsonIgnore]
        public Color LogoBackgroundColor;

        [JsonIgnore]
        public Color ButtonColor;
        [JsonIgnore]
        public Color PeriodColor;
        [JsonIgnore]
        public Color PeriodFavoriteColor;

        [JsonIgnore]
        public Color TimegridBackground;
        #endregion

        [JsonProperty]
        public double DayWidth = 250;

        [JsonProperty(IsReference = true)]
        private BetterUntisUser _currentUser;

        public Config()
        {
            Users = new ObservableCollection<BetterUntisUser>();
        }

        public event EventHandler CurrentUserChanged;

        public static Config Current { get; private set; }

        [JsonProperty(ItemIsReference = true)]
        public ObservableCollection<BetterUntisUser> Users { get; private set; }

        public BetterUntisUser CurrentUser
        {
            get => _currentUser;
            set
            {
                if (value == _currentUser)
                    return;

                _currentUser = value;
                CurrentUserChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        [JsonProperty]
        public int Theme { get; set; }

        public static void Load()
        {
            try
            {
                var text = FileSystem.LoadFile(ConfigFile);

                if (!String.IsNullOrEmpty(text))
                {
                    Current = JsonConvert.DeserializeObject<Config>(text);
                    return;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            Current = new Config();
        }

        public void Save()
        {
            try
            {
                FileSystem.SaveFile(ConfigFile, JsonConvert.SerializeObject(this));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public void AddUser(BetterUntisUser user)
        {
            var index = 0;

            while (index < Users.Count && user.DisplayName.CompareTo(Users[index].DisplayName) > 0)
                index++;

            Users.Insert(index, user);
        }

        public bool DeleteUser(BetterUntisUser user)
        {
            if (user == CurrentUser)
            {
                try
                {
                    CurrentUser.User.Logout().Wait();
                }
                catch
                {
                }
                CurrentUser.Dispose();
            }

            var deleted = Users.Remove(user);

            if (deleted)
                Save();

            return deleted;
        }

        public void LoadTheme(int theme = -1)
        {
            if (theme >= 0 && theme < 4)
                Theme = theme;

            switch (Theme)
            {
                case (0): //white
                    DetailPageTextColor = Color.FromRgb(10, 10, 10);
                    DetailPageBackgroundColor = Color.FromHex("ecf0f1");

                    MasterPageTextColor = Color.FromRgb(10, 10, 10);
                    MasterPageBackgroundColor = Color.FromRgb(238, 242, 222);

                    LogoBackgroundColor = Color.FromRgb(231, 242, 186);

                    ButtonColor = Color.FromHex("95a5a6");
                    PeriodColor = Color.FromRgb(200, 180, 170);
                    PeriodFavoriteColor = Color.FromHex("bdc3c7");

                    TimegridBackground = Color.FromHex("dad9d1");
                    break;
                case (1): // blue
                    DetailPageTextColor = Color.FromRgb(43, 44, 45);
                    DetailPageBackgroundColor = Color.FromRgb(104, 147, 238);

                    MasterPageTextColor = Color.FromRgb(43, 44, 45);
                    MasterPageBackgroundColor = Color.FromRgb(193, 212, 251);

                    LogoBackgroundColor = Color.FromRgb(134, 171, 249);

                    ButtonColor = Color.FromRgb(193, 212, 251);
                    PeriodColor = Color.FromRgb(162, 194, 251);
                    PeriodFavoriteColor = Color.FromRgb(208, 223, 255);

                    TimegridBackground = Color.FromRgb(134, 171, 249);
                    break;
                case (2): // Green
                    DetailPageTextColor = Color.FromRgb(55, 71, 79);
                    DetailPageTextColor = Color.FromRgb(43, 44, 45);
                    DetailPageBackgroundColor = Color.FromRgb(220, 237, 200);

                    MasterPageTextColor = Color.FromRgb(43, 44, 45);
                    MasterPageBackgroundColor = Color.FromRgb(129, 199, 132);

                    LogoBackgroundColor = Color.FromRgb(156, 204, 101);

                    ButtonColor = Color.FromRgb(102, 187, 106);
                    PeriodColor = Color.FromRgb(68, 145, 80);
                    PeriodFavoriteColor = Color.FromRgb(180, 205, 57);

                    TimegridBackground = Color.FromRgb(165, 214, 167);
                    break;
            }

            App.Current.Resources["DetailPageBackgroundColor"] = DetailPageBackgroundColor;
            App.Current.Resources["MasterPageBackgroundColor"] = MasterPageBackgroundColor;
            App.Current.Resources["DetailPageTextColor"] = DetailPageTextColor;
            App.Current.Resources["MasterPageTextColor"] = MasterPageTextColor;
            App.Current.Resources["LogoBackgroundColor"] = LogoBackgroundColor;
            App.Current.Resources["ButtonColor"] = ButtonColor;
            App.Current.Resources["PeriodColor"] = PeriodColor;
            App.Current.Resources["PeriodFavoriteColor"] = PeriodFavoriteColor;
            App.Current.Resources["TimegridBackground"] = TimegridBackground;
        }

        public void setDayWidth(int i)
        {
            switch (i)
            {
                case 0:
                    DayWidth = 250;
                    break;
                case 1:
                    DayWidth = 125;
                    break;
                case 2:
                    DayWidth = 80;
                    break;
            }
        }
    }
}
