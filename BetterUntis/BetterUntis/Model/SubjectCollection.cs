﻿using BetterUntis.Untis.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BetterUntis.Model
{
    public class SubjectCollection
    {
        [JsonProperty]
        private Dictionary<long, Class[]> _subjects = new Dictionary<long, Class[]>();

        public Dictionary<long, Class[]> Subjects => _subjects;

        public bool Contains(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));
            if ((period.Su?.Length ?? 0) == 0 || (period.Kl?.Length ?? 0) == 0)
                return false;

            return _subjects.ContainsKey(CreateSubjectId(period));
        }

        private static long CreateSubjectId(Period period)
        {
            var a = period.Su.First().Id;
            var b = period.Te.FirstOrDefault()?.Id ?? 0;

            return (long)(uint)a << 32 | (uint)b;
        }

        public void Add(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));
            if (period.Su == null || period.Su.Length == 0)
                throw new ArgumentException("Period must contain at least one subject.", nameof(period));
            if (period.Kl == null || period.Kl.Length == 0)
                throw new ArgumentException("Period must contain at least one class.");

            _subjects[CreateSubjectId(period)] = period.Kl;
        }

        public void Remove(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));
            if (period.Su == null || period.Su.Length == 0)
                throw new ArgumentException("Period must contain at least one subject.", nameof(period));
            if (period.Kl == null || period.Kl.Length == 0)
                throw new ArgumentException("Period must contain at least one class.");

            _subjects.Remove(CreateSubjectId(period));
        }

        public void Clear()
        {
            _subjects.Clear();
        }

        public async Task<Period[]> GetTimetable(BetterUntisUser user, DateTime startDate, DateTime endDate)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            if (endDate < startDate)
                throw new ArgumentOutOfRangeException(nameof(endDate));

            var subjectsLeft = new List<KeyValuePair<long, Class[]>>(_subjects);
            var periods = new List<Period>();

            while (subjectsLeft.Count > 0)
            {
                var firstSubject = subjectsLeft.First();
                var firstSubjectClass = firstSubject.Value.First();
                var timetable = (await user.GetTimetable(firstSubjectClass, startDate, endDate).ConfigureAwait(false))
                    .Where(x => Contains(x));

                periods.AddRange(timetable);
                subjectsLeft.RemoveAll(x => x.Value.Any(y => y.Id == firstSubjectClass.Id));
            }

            return periods.ToArray();
        }
    }
}
