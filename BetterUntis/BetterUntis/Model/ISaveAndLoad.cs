﻿namespace WorkingWithFiles
{
    public interface ISaveAndLoad
    {
        void Save(string fileName, string text);
        string Load(string fileName);
    }
}
