﻿using BetterUntis.Untis.Models;
using BetterUntis.Untis.Time;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WebUntisAPI.Models;
using static BetterUntis.JsonRpc.JsonRpc;

namespace BetterUntis.Untis
{
    public sealed class UntisUser : IDisposable
    {
        private readonly HttpClient _client;

        private UntisAuthentication _authentication;
        private string _session;

        [JsonConstructor]
        public UntisUser(UntisAuthentication authentication)
        {
            _client = new HttpClient();
            _authentication = authentication;
            _session = null;
        }

        public UntisAuthentication Authentication
        {
            get => _authentication;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));
                if (LoggedIn)
                    throw new InvalidOperationException("Unable to change credentials while logged in.");

                _authentication = value;
                _session = null;
            }
        }

        public bool LoggedIn => _session != null;

        public async Task Login()
        {
            if (_session != null)
                _client.Dispose();

            _session = await Authentication.CreateSession(_client).ConfigureAwait(false);
        }

        public async Task Logout()
        {
            if (_session != null)
            {
                try
                {
                    await SendASync("logout").ConfigureAwait(false);
                }
                catch (JsonRpcException ex) when (ex.Code == JsonRpcException.NotAuthenticated)
                {
                }
                finally
                {
                    _session = null;
                }
            }
        }


        public Task<Teacher[]> GetTeachers()
        {
            return GetElement<Teacher[]>("getTeachers");
        }

        public Task<Student[]> GetStudents()
        {
            return GetElement<Student[]>("getStudents");
        }

        public Task<Class[]> GetClasses()
        {
            return GetElement<Class[]>("getKlassen");
        }

        public Task<Subject[]> GetSubjects()
        {
            return GetElement<Subject[]>("getSubjects");
        }

        public Task<Room[]> GetRooms()
        {
            return GetElement<Room[]>("getRooms");
        }

        public Task<Department[]> GetDepartments()
        {
            return GetElement<Department[]>("getDepartments");
        }

        public Task<Holiday[]> GetHolidays()
        {
            return GetElement<Holiday[]>("getHolidays");
        }

        public Task<Day[]> GetTimegrid()
        {
            return GetElement<Day[]>("getTimegridUnits");
        }

        public Task<SchoolYear> GetCurrentSchoolYear()
        {
            return GetElement<SchoolYear>("getCurrentSchoolyear");
        }

        public Task<SchoolYear[]> GetSchoolYears()
        {
            return GetElement<SchoolYear[]>("getSchoolyears");
        }

        public async Task<Period[]> GetTimetable(UntisElement element, DateTime startDate, DateTime endDate)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));
            if (startDate > endDate)
                throw new ArgumentOutOfRangeException(nameof(endDate), "endDate must be greater than startDate.");

            CheckLoggedIn();

            var parameters = new JObject(
                new JProperty("options",
                    new JObject(
                        new JProperty("element",
                            new JObject(
                                new JProperty("id", element.Id),
                                new JProperty("type", element.Type))),
                        new JProperty("startDate", UntisDate.FromDateTime(startDate)),
                        new JProperty("endDate", UntisDate.FromDateTime(endDate)),
                        new JProperty("showInfo", true),
                        new JProperty("showSubstText", true),
                        new JProperty("showLsText", true),
                        new JProperty("showLsNumber", true),
                        new JProperty("showStudentgroup", true),
                        new JProperty("klasseFields",
                            new JArray(
                                new JValue("id"),
                                new JValue("name"),
                                new JValue("longname"))),
                        new JProperty("roomFields",
                            new JArray(
                                new JValue("id"),
                                new JValue("name"),
                                new JValue("longname"))),
                        new JProperty("subjectFields",
                            new JArray(
                                new JValue("id"),
                                new JValue("name"),
                                new JValue("longname"))),
                        new JProperty("teacherFields",
                            new JArray(
                                new JValue("id"),
                                new JValue("name"),
                                new JValue("longname"))))));

            return (await SendASync("getTimetable", parameters).ConfigureAwait(false)).ToObject<Period[]>();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        private void CheckLoggedIn()
        {
            if (!LoggedIn)
                throw new InvalidOperationException("A login is required in order to execute this method.");
        }

        private async Task<T> GetElement<T>(string method)
        {
            CheckLoggedIn();

            try
            {
                return (await SendASync(method).ConfigureAwait(false)).ToObject<T>();
            }
            catch (JsonRpcException ex) when (ex.Code == JsonRpcException.NotAuthenticated)
            {
                _session = null;
                throw;
            }
        }

        private async Task<JToken> SendASync(string method, JToken parameters = null)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Post, _authentication.ServiceUrl))
            {
                request.Headers.Add("JSESSIONID", _session);
                request.Content = CreateRequestContent("ID", method, parameters);

                var response = await _client.SendAsync(request).ConfigureAwait(false);
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                return ParseResponseContent(content);
            }
        }
    }
}
