﻿namespace BetterUntis.Untis.Models
{
    public enum UntisElementType
    {
        Class = 1,
        Teacher = 2,
        Subject = 3,
        Room = 4,
        Student = 5
    }
}
