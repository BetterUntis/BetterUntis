﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Subject : UntisElement
    {
        [JsonConstructor]
        private Subject(int id, string name, string longName) : base(id, name, longName)
        {
        }

        public override UntisElementType Type => UntisElementType.Subject;
    }
}
