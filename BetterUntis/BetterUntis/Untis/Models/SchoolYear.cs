﻿using Newtonsoft.Json;

namespace WebUntisAPI.Models
{
    public class SchoolYear
    {
        [JsonConstructor]
        private SchoolYear(int id, string name, int startDate, int endDate)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            EndDate = endDate;
        }

        [JsonProperty(PropertyName="id")]
        public int Id { get; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; }
        
        [JsonProperty(PropertyName = "startDate")]
        public int StartDate { get; }
        
        [JsonProperty(PropertyName = "endDate")]
        public int EndDate { get; }
    }
}
