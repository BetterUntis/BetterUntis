﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Teacher : UntisElement
    {
        [JsonConstructor]
        private Teacher(int id, string name, string foreName, string longName)
            : base(id, name, longName)
        {
            ForeName = foreName;
        }        

        [JsonProperty(PropertyName = "foreName")]
        public string ForeName { get; }

        public override UntisElementType Type => UntisElementType.Teacher;
    }
}
