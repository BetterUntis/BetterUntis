﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public abstract class UntisElement
    {
        protected UntisElement(int id, string name, string longName)
        {
            Id = id;
            Name = name;
            LongName = longName;
        }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; }

        [JsonProperty(PropertyName = "longName")]
        public string LongName { get; }

        public string Description => $"{Name} ({LongName})";
        public abstract UntisElementType Type { get; }
    }
}
