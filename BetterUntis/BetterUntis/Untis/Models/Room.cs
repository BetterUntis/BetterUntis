﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Room : UntisElement
    {
        [JsonConstructor]
        private Room(int id, string name, string longName, string building) :base(id,name,longName)
        {
            Building = building;
        }

        [JsonProperty(PropertyName="building")]
        public string Building { get; }

        public override UntisElementType Type => UntisElementType.Room;
    }
}
