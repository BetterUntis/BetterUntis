﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    [JsonObject(IsReference = true)]
    public class Class : UntisElement
    {
        [JsonConstructor]
        private Class(int id, string name, string longName) : base(id,name,longName)
        {
        }

        public override UntisElementType Type => UntisElementType.Class;
    }
}
