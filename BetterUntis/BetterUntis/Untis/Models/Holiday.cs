﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Holiday
    {
        [JsonConstructor]
        private Holiday(int id, string name, string longName, string startDate, string endDate)
        {
            Id = id;
            Name = name;
            LongName = longName;
            StartDate = startDate;
            EndDate = endDate;
        }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; }

        [JsonProperty(PropertyName = "longName")]
        public string LongName { get; }

        [JsonProperty(PropertyName = "startDate")]
        public string StartDate { get; }

        [JsonProperty(PropertyName = "endDate")]
        public string EndDate { get; }
    }
}
