﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Day
    {
        [JsonConstructor]
        private Day(int dayOfWeek, TimeRange[] timeRanges)
        {
            DayOfWeek = dayOfWeek;
            TimeRanges = timeRanges;
        }

        [JsonProperty(PropertyName = "day")]
        public int DayOfWeek { get; }

        [JsonProperty(PropertyName ="timeUnits")]
        public TimeRange[] TimeRanges { get; }
    }
}
