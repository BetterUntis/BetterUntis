﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Student : UntisElement
    {
        [JsonConstructor]
        private Student(int id, string name, string longName, string foreName) : base(id, name, longName)
        {
            ForeName = foreName;
        }

        [JsonProperty(PropertyName = "foreName")]
        public string ForeName { get; }

        public override UntisElementType Type => UntisElementType.Student;
    }
}
