﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class Period
#if DEBUG
        : System.IEquatable<Period>
        #endif
    {
#if DEBUG
        public bool Equals(Period other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Lstype, other.Lstype) && string.Equals(Code, other.Code) && string.Equals(Info, other.Info) && string.Equals(SubstText, other.SubstText) && string.Equals(Lstext, other.Lstext) && Lsnumber == other.Lsnumber && string.Equals(Statflags, other.Statflags) && string.Equals(Sg, other.Sg)
                && ArrayEquals(Kl, other.Kl)
                && ArrayEquals(Te, other.Te)
                && ArrayEquals(Su, other.Su)
                && ArrayEquals(Ro, other.Ro);
        }

        public static bool ArrayEquals<T>(T[] a, T[] b)
        {
            if (a == b)
                return true;

            return a.Length == b.Length;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Period)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Lstype != null ? Lstype.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Code != null ? Code.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Info != null ? Info.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SubstText != null ? SubstText.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Lstext != null ? Lstext.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Lsnumber;
                hashCode = (hashCode * 397) ^ (Statflags != null ? Statflags.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Sg != null ? Sg.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Kl != null ? Kl.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Te != null ? Te.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Su != null ? Su.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Ro != null ? Ro.GetHashCode() : 0);
                return hashCode;
            }
        }
#endif

        [JsonConstructor]
        private Period(int id,
                       int date,
                       int startTime,
                       int endTime,
                       string lstype,
                       string code,
                       string info,
                       string substText,
                       string lstext,
                       int lsnumber,
                       string statflags,
                       string sg,
                       Class[] kl,
                       Teacher[] te,
                       Subject[] su,
                       Room[] ro)
        {
            Id = id;
            Date = date;
            StartTime = startTime;
            EndTime = endTime;
            Lstype = lstype;
            Code = code;
            Info = info;
            SubstText = substText;
            Lstext = lstext;
            Lsnumber = lsnumber;
            Statflags = statflags;
            Sg = sg;
            Kl = kl;
            Te = te;
            Su = su;
            Ro = ro;
        }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; }

        [JsonProperty(PropertyName = "date")]
        public int Date { get; }

        [JsonProperty(PropertyName = "startTime")]
        public int StartTime { get; }

        [JsonProperty(PropertyName = "endTime")]
        public int EndTime { get; }

        [JsonProperty(PropertyName = "lstype")]
        public string Lstype { get; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; }

        [JsonProperty(PropertyName = "info")]
        public string Info { get; }

        [JsonProperty(PropertyName = "substText")]
        public string SubstText { get; }

        [JsonProperty(PropertyName = "lstext")]
        public string Lstext { get; }

        [JsonProperty(PropertyName = "lsnumber")]
        public int Lsnumber { get; }

        [JsonProperty(PropertyName = "statflags")]
        public string Statflags { get; }

        [JsonProperty(PropertyName = "sg")]
        public string Sg { get; }

        [JsonProperty(PropertyName = "kl")]
        public Class[] Kl { get; }

        [JsonProperty(PropertyName = "te")]
        public Teacher[] Te { get; }

        [JsonProperty(PropertyName = "su")]
        public Subject[] Su { get; }

        [JsonProperty(PropertyName = "ro")]
        public Room[] Ro { get; }
    }
}