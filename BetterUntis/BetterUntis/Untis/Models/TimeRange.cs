﻿using Newtonsoft.Json;

namespace BetterUntis.Untis.Models
{
    public class TimeRange
    {
        [JsonConstructor]
        private TimeRange(string name, int startTime, int endTime)
        {
            Name = name;
            StartTime = startTime;
            EndTime = endTime;
        }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; }

        [JsonProperty(PropertyName = "startTime")]
        public int StartTime { get; set; }

        [JsonProperty(PropertyName = "endTime")]
        public int EndTime { get; set; }
    }
}
