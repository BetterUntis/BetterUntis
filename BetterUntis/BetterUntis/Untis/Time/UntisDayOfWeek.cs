﻿using System;

namespace BetterUntis.Untis.Time
{
    public static class UntisDayOfWeek
    {
        public static DayOfWeek ToDayOfWeek(int untisDay)
        {
            if (untisDay > ((int)DayOfWeek.Saturday + 1) || untisDay < ((int)DayOfWeek.Sunday + 1))
                throw new ArgumentOutOfRangeException(nameof(untisDay));

            return (DayOfWeek)(untisDay - 1);
        }

        public static int FromDayOfWeek(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek < DayOfWeek.Sunday || dayOfWeek > DayOfWeek.Saturday)
                throw new ArgumentOutOfRangeException(nameof(dayOfWeek));

            return (int)dayOfWeek + 1;
        }
    }
}
