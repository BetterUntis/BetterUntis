﻿using System;

namespace BetterUntis.Untis.Time
{
    public static class UntisTime
    {
        public static TimeSpan ToTimeSpan(int untisTimestamp)
        {
            return new TimeSpan(untisTimestamp / 100, untisTimestamp % 100, 0);
        }

        public static int FromTimeSpan(TimeSpan timeSpan)
        {
            return timeSpan.Hours * 100 + timeSpan.Minutes;
        }
    }
}
