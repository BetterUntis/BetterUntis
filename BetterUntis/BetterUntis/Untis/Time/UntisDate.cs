﻿using System;

namespace BetterUntis.Untis.Time
{
    public static class UntisDate
    {
        public static DateTime ToDateTime(int webUntisTimestamp)
        {
            return new DateTime(webUntisTimestamp / 100 / 100, webUntisTimestamp / 100 % 100, webUntisTimestamp % 100);
        }
        
        public static int FromDateTime(DateTime dateTime)
        {
            return dateTime.Year * 100 * 100 + dateTime.Month * 100 + dateTime.Day;
        }
    }
}
