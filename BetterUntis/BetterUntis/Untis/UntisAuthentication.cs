﻿using BetterUntis.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PCLCrypto;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using static PCLCrypto.WinRTCrypto;
using static BetterUntis.JsonRpc.JsonRpc;
using System.Text.RegularExpressions;

namespace BetterUntis.Untis
{
    public class UntisAuthentication
    {
        public const string AnonymousUser = "#anonymous#";
        public const string Client = "CLIENT";

        [JsonConstructor]
        public UntisAuthentication(string school, string url, string username, string password, bool passwordIsKey)
        {
            if (school == null)
                throw new ArgumentNullException(nameof(school));
            if (url == null)
                throw new ArgumentNullException(nameof(url));
            if (username == null)
                throw new ArgumentNullException(nameof(username));
            if (password == null)
                throw new ArgumentNullException(nameof(password));

            School = school;
            Url = url;
            Username = username;
            Password = password;
            PasswordIsKey = passwordIsKey;
        }

        public string School { get; }
        public string Url { get; }
        public string Username { get; }
        public string Password { get; }
        public bool PasswordIsKey { get; }

        [JsonIgnore]
        public string ServiceUrl => $"https://{Url}/WebUntis/jsonrpc.do?school={School}";
        [JsonIgnore]
        public string ServiceUrlIntern => $"https://{Url}/WebUntis/jsonrpc_intern.do?school={School}";

        public static UntisAuthentication CreateAnonymous(string school, string url)
        {
            return new UntisAuthentication(school, url, AnonymousUser, string.Empty, true);
        }

        public static UntisAuthentication CreateWithKey(string school, string url, string username, string key)
        {
            return new UntisAuthentication(school, url, username, key, true);
        }

        public static UntisAuthentication CreateWithPassword(string school, string url, string username, string password)
        {
            return new UntisAuthentication(school, url, username, password, false);
        }

        public Task<string> CreateSession(HttpClient client)
        {
            if (PasswordIsKey)
                return CreateSessionWithKey(client);

            return CreateSessionWithPassword(client);
        }
        
        private async Task<string> CreateSessionWithKey(HttpClient client)
        {
            var unixTime = DateTime.UtcNow.ToUnixTimeMilliseconds();
            var oneTimePassword = Username == AnonymousUser ? string.Empty : CreateOneTimePassword(unixTime);

            var credentials = new JArray(
                new JObject(
                    new JProperty("auth",
                                  new JObject(
                                      new JProperty("user", Username),
                                      new JProperty("otp", oneTimePassword),
                                      new JProperty("clientTime", unixTime))),
                    new JProperty("masterDataTimestamp", 0)));

            using (var request = new HttpRequestMessage(HttpMethod.Post, ServiceUrlIntern))
            {
                request.Content = CreateRequestContent("ID", "getUserData", credentials);

                var response = await client.SendAsync(request).ConfigureAwait(false);

                if (response.Headers.TryGetValues("Set-Cookie", out var values))
                {
                    var regex = new Regex("JSESSIONID=([0-9A-Za-z]+);");

                    foreach (var value in values)
                    {
                        var match = regex.Match(value);

                        if (match.Success)
                            return match.Groups[1].Value;
                    }
                }

                throw JsonRpcException.FromParseError("No session found.");
            }
        }

        private async Task<string> CreateSessionWithPassword(HttpClient client)
        {
            var credentials = new JObject(
                new JProperty("user", Username),
                new JProperty("password", Password),
                new JProperty("client", Client));

            JToken result;

            using (var request = new HttpRequestMessage(HttpMethod.Post, ServiceUrl))
            {
                request.Content = CreateRequestContent("ID", "authenticate", credentials);

                var response = await client.SendAsync(request).ConfigureAwait(false);
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                result = ParseResponseContent(content);
            }
            
            var session = (string)result["sessionId"];

            if (session == null)
                throw JsonRpcException.FromParseError("No session found.");

            return session;
        }
        
        private string CreateOneTimePassword(long unixTimestamp)
        {
            var apiKey = ConvertEx.FromBase32String(Password);
            var data = BitConverterEx.GetUInt64BE(unixTimestamp / 30000);
            var hash = ComputeHmacSha1(apiKey, data);

            return DeriveOneTimePassword(hash);
        }

        private string DeriveOneTimePassword(byte[] hash)
        {
            var offset = hash[hash.Length - 1] & 0x0F;

            var res = (((hash[offset + 0] & 0x7f) << 24) |
                      ((hash[offset + 1] & 0xff) << 16) |
                      ((hash[offset + 2] & 0xff) << 8) |
                      (hash[offset + 3] & 0xff)) % 1000000;

            return res.ToString("D6");
        }

        private static byte[] ComputeHmacSha1(byte[] key, byte[] data)
        {
            var hmac = MacAlgorithmProvider.OpenAlgorithm(MacAlgorithm.HmacSha1);
            byte[] hash;

            using (var hmacKey = hmac.CreateHash(key))
            {
                hmacKey.Append(data);
                hash = hmacKey.GetValueAndReset();
            }
            return hash;
        }
    }
}
