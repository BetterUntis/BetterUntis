﻿namespace BetterUntis.Untis
{
    public enum UntisCachePolicy
    {
        OfflineOnly,
        RecentCache,
        AlwaysRecent,
    }
}
