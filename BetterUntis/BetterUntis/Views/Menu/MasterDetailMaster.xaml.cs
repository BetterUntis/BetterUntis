﻿using BetterUntis.Model;
using BetterUntis.Views.DetailView;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterUntis.Views.Menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailMaster : ContentPage
    {
        public ListView ListView;
        public Picker Users;
        
        public MasterDetailMaster()
        {
            InitializeComponent();
            Pckr_Users.SelectedItem = null;
            BindingContext = new MasterDetailMasterViewModel();
            ListView = MenuItemsListView;
            MenuItemsListView.SeparatorVisibility = Device.RuntimePlatform == Device.Android ? SeparatorVisibility.Default : SeparatorVisibility.None;

            Users = Pckr_Users;
            
            Pckr_Users.ItemsSource = Config.Current.Users;

            if (Pckr_Users.ItemsSource.Count > 0)
                Pckr_Users.SelectedItem = Config.Current.CurrentUser ?? Config.Current.Users.FirstOrDefault();

            Config.Current.CurrentUserChanged += Current_CurrentUserChanged;
            Users.SelectedIndexChanged += Users_SelectedIndexChanged;
        }

        private void Current_CurrentUserChanged(object sender, EventArgs e)
        {
            Pckr_Users.SelectedItem = Config.Current.CurrentUser;
        }

        private void Users_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedUser = (BetterUntisUser)Pckr_Users.SelectedItem;

            if (selectedUser != Config.Current.CurrentUser)
            {
                Config.Current.CurrentUser = selectedUser;
            //    Config.Current.Save();
            }
        }

        public class MasterDetailMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterDetailMenuItem> MenuItems { get; set; }

            public MasterDetailMasterViewModel()
            {                
                MenuItems = new ObservableCollection<MasterDetailMenuItem>(new[]
                {
                    new MasterDetailMenuItem { Id = 0, Title = "My Timetable", RequiresUser = true, TargetType=typeof(TimeTablePage) },
                    new MasterDetailMenuItem { Id = 1, Title = "Browse", RequiresUser = true, TargetType=typeof(BrowsePage) },
                    new MasterDetailMenuItem { Id = 2, Title = "Search Free Room", RequiresUser = true, TargetType=typeof(SearchFreeRoom) },
                    new MasterDetailMenuItem { Id = 3, Title = "Search Teacher", RequiresUser = true, TargetType=typeof(SearchTeacher) },
                    new MasterDetailMenuItem { Id = 4, Title = "Settings", RequiresUser = true, TargetType=typeof(SettingsPage) },
                    new MasterDetailMenuItem { Id = 5, Title = "User menu", RequiresUser = false, TargetType=typeof(UserPage) },
                });          
            }
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}