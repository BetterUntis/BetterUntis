﻿using BetterUntis.Model;
using BetterUntis.Untis.Models;
using BetterUntis.Views.DetailView;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterUntis.Views.Menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetail : MasterDetailPage
    {
        private List<UntisElement> _elements = new List<UntisElement>();

        public MasterDetail()
        {
            InitializeComponent();
            
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;

            if (Config.Current.CurrentUser != null)
            {
                Detail = new NavigationPage(new TimeTablePage());
            }
            else
            {
                Detail = new NavigationPage(new UserPage());
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterDetailMenuItem;

            if (item == null)
                return;

            if (item.RequiresUser)
            {
                var currentUser = Config.Current.CurrentUser;

                if (currentUser == null)
                {
                    DisplayAlert("Error", "You must be logged in to view this page.", "OK");
                    return;
                }
            }

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;
            Detail = new NavigationPage(page);
            IsPresented = false;
            MasterPage.ListView.SelectedItem = null;
        }
    }
}