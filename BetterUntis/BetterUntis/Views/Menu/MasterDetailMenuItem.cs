﻿using BetterUntis.Model;
using System;
using Xamarin.Forms;

namespace BetterUntis.Views.Menu
{

    public class MasterDetailMenuItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool RequiresUser { get; set; }

        public Color CurrentBackgroundColor
        {
            get
            {
                var color = (Color) App.Current.Resources["MasterPageBackgroundColor"];

                if (RequiresUser && Config.Current.CurrentUser == null)
                    return (Color)App.Current.Resources["MasterPageDisabledBackgroundColor"];

                return (Color)App.Current.Resources["MasterPageBackgroundColor"];
            }
        }


        public Type TargetType { get; set; }
    }
}