﻿using BetterUntis.Extensions;
using BetterUntis.Model;
using BetterUntis.Untis.Models;
using BetterUntis.Untis.Time;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchTeacher : ContentPage
    {
        private string _teacherName;
        private DateTime _startDate;
        private DateTime _endDate;

        private bool _busy;
        private bool _cancel;

        public SearchTeacher()
        {
            InitializeComponent();

            DP_StartTime.Date = DateTime.Today.StartOfWeek(DayOfWeek.Monday);
            DP_EndTime.Date = DP_StartTime.Date.AddDays(6);

        }

        private async void Btn_Search_Clicked(object sender, EventArgs e)
        {
            if (_busy)
            {
                _cancel = true;
                Btn_Search.Text = "Cancelling...";
                return;
            }

            if (DP_EndTime.Date < DP_StartTime.Date)
            {
                await DisplayAlert("Invalid end date", "End time must be greater than start time.", "OK");
                return;
            }

            if (DP_EndTime.Date - DP_StartTime.Date > TimeSpan.FromDays(2*7))
            {
                await DisplayAlert("Invalid time span", "Time span must not be longer than 2 weeks.", "OK");
                return;
            }

            if (String.IsNullOrEmpty(Entry_TeacherName.Text))
            {
                await DisplayAlert("Invalid teacher name", "Teacher name must not be empty.", "OK");
                return;
            }
            
            _teacherName = Entry_TeacherName.Text.Trim();
            _startDate = DP_StartTime.Date;
            _endDate = DP_EndTime.Date;
            _busy = true;

            Btn_Search.Text = "Cancel";
            PB_Loading.IsVisible = PB_Loading.IsEnabled = true;
            PB_Loading.Progress = 0.0;

            retry:
            try
            {
                var rooms = await Config.Current.CurrentUser.GetRooms();
                var dict = new Dictionary<int, List<IGrouping<int, Period>>>();

                for (int i = 0; i < rooms.Length; i++)
                {
                    if (_cancel)
                        break;

                    foreach (var lesson in (await Config.Current.CurrentUser.GetTimetable(rooms[i], _startDate, _endDate))
                        .Where(x => x.Te.Any(y => DisplayTeacher(y)))
                        .GroupBy(x => x.Lsnumber))
                    {
                        int date = lesson.Min(x => x.Date);

                        if (dict.TryGetValue(date, out var list))
                            list.Add(lesson);
                        else
                            dict.Add(date, new List<IGrouping<int, Period>> { lesson });
                    }

                    PB_Loading.Progress = (double)(i + 1) / rooms.Length;
                }

                var root = new TableRoot($"Search for \"{_teacherName}\" from {_startDate:dd. MMMM yyyy} to {_endDate:dd. MMMM yyyy}");

                foreach (var pair in dict.OrderBy(x => x.Key))
                {
                    var section = new TableSection(UntisDate.ToDateTime(pair.Key).ToString("dd. MMMM yyyy"));

                    foreach (var lesson in pair.Value.OrderBy(x => x.Min(y => y.StartTime)))
                        section.Add(CreateSection(lesson));

                    root.Add(section);
                }

                TV_Periods.Root = root;
            }
            catch (Exception ex)
            {
                if (await DisplayAlert("Failed to fetch data",
                                       $"Failed to fetch data from the untis server:{Environment.NewLine}{Environment.NewLine}{ex.Message}",
                                       "Retry",
                                       "Cancel"))
                {
                    goto retry;
                }
            }

            Btn_Search.Text = "Search";
            PB_Loading.IsVisible = PB_Loading.IsEnabled = false;
            _busy = _cancel = false;
        }

        private static TextCell CreateSection(IGrouping<int, Period> lesson)
        {
            // 8:00
            // Lesson:
            //
            /*return new TextCell
            {
                Text = $"{UntisTime.ToTimeSpan(lesson.Min(x => x.StartTime)):hh\\:mm}",

                Detail = $"Subject:\t{String.Join(", ", lesson.First().Su.Select(x => x.Name))}" + Environment.NewLine +
                         $"Teacher:\t{String.Join(", ", lesson.First().Te.Select(x => x.Description))}" + Environment.NewLine +
                         $"Room:\t{String.Join(", ", lesson.First().Ro.Select(x => x.Name))}"
            };*/
            
            return new TextCell
            {
                Text = $"{UntisTime.ToTimeSpan(lesson.Min(x => x.StartTime)):hh\\:mm} - {String.Join(", ", lesson.First().Su.Select(x => x.Name))} [{String.Join(", ", lesson.First().Te.Select(x => x.LongName))}]",
                Detail = String.Join(", ", lesson.First().Ro.Select(x => x.Name))
            };
        }

        private bool DisplayTeacher(Teacher teacher)
        {
            if (teacher.Name.IndexOf(_teacherName, StringComparison.OrdinalIgnoreCase) >= 0)
                return true;

            if (teacher.LongName.IndexOf(_teacherName, StringComparison.OrdinalIgnoreCase) >= 0)
                return true;

            return false;
        }
    }
}