﻿using BetterUntis.Model;
using BetterUntis.Untis.Models;
using BetterUntis.Untis.Time;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchFreeRoom : ContentPage
    {
        private Room[] _rooms;
        private List<string> _buildings = new List<string>();

        private char _separator;

        private bool _busy = false;
        private bool _cancel = false;

        public ObservableCollection<Room> Elements { get; } = new ObservableCollection<Room>();

        public SearchFreeRoom()
        {
            InitializeComponent();


            Dpckr_Day.Date = DateTime.Now;
            Tpckr_TimeFrom.Time = DateTime.Now.TimeOfDay;
            Tpckr_TimeTo.Time = DateTime.Now.TimeOfDay;

            ListView_Search.ItemsSource = Elements;

        }

        protected async override void OnAppearing()
        {
            if (_rooms != null)
                return;

            var user = Config.Current.CurrentUser;
            Debug.Assert(user != null);

            AI_IsSeraching.IsRunning = false;

            retry:
            try
            {
                _rooms = await user.GetRooms();
            }
            catch (Exception ex)
            {
                if (await DisplayAlert("Failed to fetch data",
                                       $"Failed to fetch data from the untis server:{Environment.NewLine}{Environment.NewLine}{ex.Message}",
                                       "Retry",
                                       "Cancel"))
                {
                    goto retry;
                }
                return;
            }

            if (Config.Current.GuessBuildings)
                _buildings = GuessBuildings();

            if (_buildings.Count == 0)
                _buildings.AddRange(_rooms.Select(x => x.Building).Where(x => x.Length > 0).Distinct());

            _buildings.Insert(0, "(All)");

            Pckr_Building.ItemsSource = _buildings;
            Pckr_Building.SelectedIndex = 0;
        }

        protected override void OnDisappearing()
        {
            _cancel = true;

            base.OnDisappearing();
        }

        private async void Search_SearchStarted(object sender, EventArgs e)
        {
            if (_rooms == null)
            {
                await DisplayAlert("No data available", "No data available for searching.", "OK");
                return;
            }

            if (_busy)
            {
                Btn_StartSearch.Text = "Cancelling...";
                _cancel = true;
            }
            else
            {
                var start = Tpckr_TimeFrom.Time;
                var end = Tpckr_TimeTo.Time;

                if (end < start)
                {
                    await DisplayAlert("Invalid range", "End time must be greater than start time.", "OK");
                    return;
                }

                Btn_StartSearch.Text = "Cancel";
                AI_IsSeraching.IsRunning = AI_IsSeraching.IsEnabled = AI_IsSeraching.IsVisible = true;

                var user = Config.Current.CurrentUser;
                var today = DateTime.Today;
                Debug.Assert(user != null);

                _busy = true;

                Elements.Clear();

                #region Seb Hacks
                if (Config.Current.GuessBuildings)
                {
                    foreach (var room in _rooms)
                    {
                        if (!String.IsNullOrEmpty(Search_Name.Text) && room.Description.IndexOf(Search_Name.Text, StringComparison.OrdinalIgnoreCase) < 0)
                            continue;

                        if (room.Building.Length > 0 && room.Building.IndexOf((string)Pckr_Building.SelectedItem, StringComparison.OrdinalIgnoreCase) < 0)
                            continue;

                        if (room.Building.Length == 0)
                        {
                            if (Pckr_Building.SelectedIndex > 0)
                            {
                                string str = (string)Pckr_Building.SelectedItem;
                                var array = room.Name.Split(_separator);
                                if (array.Length < 2)
                                    continue;

                                var building = array[0];

                                if (building.IndexOf((string)Pckr_Building.SelectedItem, StringComparison.OrdinalIgnoreCase) < 0)
                                    continue;
                            }
                        }

                        var periods = await user.GetTimetable(room, today, today);

                        if (_cancel)
                            break;

                        if (periods.All(x => UntisTime.ToTimeSpan(x.StartTime) >= end || UntisTime.ToTimeSpan(x.EndTime) <= start))
                            Elements.Add(room);
                    }
                }
                else
                #endregion
                {
                    foreach (var room in _rooms)
                    {
                        if (!String.IsNullOrEmpty(Search_Name.Text) && room.Description.IndexOf(Search_Name.Text, StringComparison.OrdinalIgnoreCase) < 0)
                            continue;

                        if (Pckr_Building.SelectedIndex > 0 && room.Building.IndexOf((string)Pckr_Building.SelectedItem, StringComparison.OrdinalIgnoreCase) < 0)
                            continue;

                        var periods = await user.GetTimetable(room, today, today);

                        if (_cancel)
                            break;

                        if (periods.All(x => UntisTime.ToTimeSpan(x.StartTime) >= end || UntisTime.ToTimeSpan(x.EndTime) <= start))
                            Elements.Add(room);
                    }
                }
                Btn_StartSearch.Text = "Search";
                AI_IsSeraching.IsRunning = AI_IsSeraching.IsEnabled = AI_IsSeraching.IsVisible = false;
                _busy = false;
                _cancel = false;
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var associatedElement = (UntisElement)((BindableObject)sender).BindingContext;

            Navigation.PushAsync(new TimeTablePage(associatedElement));
        }

        #region  Seb Hacks
        private List<string> GuessBuildings()
        {
            HashSet<string> buildings = new HashSet<string>();
            Dictionary<string, string> dic = new Dictionary<string, string>(); // room.Name, room.Building
            HashSet<char> set = new HashSet<char>();

            foreach (var e in _rooms)
                if (!String.IsNullOrWhiteSpace(e.Building))
                {
                    buildings.Add(e.Building);
                    dic[e.Name] = e.Building;
                }

            bool buildingIsNumber = false; // if the building's name is a number or not


            foreach (KeyValuePair<string, string> v in dic)
            {
                string key = v.Key;
                if (key.Contains(v.Value)) // Cafeteria is in Building 0 but has no 0 in name
                {
                    int i = key.IndexOf(v.Value) + 1;
                    if (i < key.Length)
                    {
                        set.Add(key[i]);
                        string[] array = key.Split(key[i]);
                        if (int.TryParse(array[0], out var _))
                            buildingIsNumber = true; // could also a combination of number and letter, like L1-234 (L=Labatory)
                    }
                }
            }

            if (set.Count == 1) // every roomname has identical separators, if not function does not work
            {
                _separator = set.ToList<char>()[0];

                foreach (var room in _rooms)
                {
                    string[] array = room.Name.Split(_separator);
                    if (array.Length < 2)
                        continue;

                    if (buildingIsNumber)
                    {
                        // but has to be array[0] if building name.Length > 1 number
                        string number = "";
                        foreach (char c in array[0])
                        {
                            if (c > 47 && c < 58)
                            {
                                number += c;
                            }
                        }
                        if (number.Length > 0)
                            buildings.Add(number);
                    }
                    else
                        buildings.Add(array[0]);
                }
            }
            return buildings.ToList();
        }
        #endregion
    }
}