﻿using System;
using System.Linq;
using BetterUntis.Untis.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BetterUntis.Untis.Time;
using System.Collections.Generic;
using System.Diagnostics;
using BetterUntis.Extensions;
using System.Threading.Tasks;
using BetterUntis.Model;
using System.Threading;
using BetterUntis.Untis;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TimeTablePage : ContentPage
    {
        private const double HeightPerHour = 60;

        private static readonly Period[] EmptyPeriods = new Period[0];

        private UntisElement _element;

        private TimeSpan _startTime;
        private TimeSpan _endTime;
        private DateTime _week;

        private Label[] _weekdayLabels = new Label[7];
        private Grid[] _weekdayGrids = new Grid[7];
        private bool _loaded = false;

        private StackLayout _timelineLayout;
        private Task _timelineTask;
        private CancellationTokenSource _cts = new CancellationTokenSource();

        public TimeTablePage()
        {
            InitializeComponent();

            _week = DateTime.Today.StartOfWeek(DayOfWeek.Monday);

            _timelineLayout = new StackLayout
            {
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Fill,
                Margin = Padding = new Thickness(0),
            };

            var timeline = new BoxView
            {
                Color = Color.Red,
                HeightRequest = 2,
                Margin = new Thickness(0)
            };

            var label = new Label
            {
                TextColor = Color.Red,
            };

            _timelineLayout.Children.Add(timeline);
            _timelineLayout.Children.Add(label);
        }

        public TimeTablePage(UntisElement elem) : this()
        {
            _element = elem;

            Title = elem.Name;
        }

        protected override async void OnAppearing()
        {
            Debug.Assert(Config.Current.CurrentUser != null);

            if (_loaded)
                return;
            
            Day[] timegrid;

            retry:
            try
            {
                timegrid = await Config.Current.CurrentUser.GetTimegrid();
            }
            catch (CacheException)
            {
                await DisplayAlert("Offline cache is empty", "There is nothing in the cache for this week, disable the 'Offline Mode' in the settings.", "OK");
                return;
            }
            catch (Exception ex)
            {
                if (await DisplayAlert("Failed to fetch the timegrid",
                                       $"Failed to fetch the timegrid from the untis server:{Environment.NewLine}{Environment.NewLine}{ex.Message}",
                                       "Retry",
                                       "Cancel"))
                {
                    goto retry;
                }

                return;
            }

            _startTime = UntisTime.ToTimeSpan(timegrid.SelectMany(x => x.TimeRanges).Min(x => x.StartTime) / 100 * 100);
            _endTime = UntisTime.ToTimeSpan((timegrid.SelectMany(x => x.TimeRanges).Max(x => x.EndTime) + 99) / 100 * 100);

            DrawTimeLabels();
            DrawWeekdayLabels();

            _loaded = true;

            await RefreshTimetable();
            await (_timelineTask = StartTimelineTask());
        }

        protected override void OnDisappearing()
        {
            _cts.Cancel();
        }

        private async Task StartTimelineTask()
        {
            var token = _cts.Token;
            DateTime now;

            for (;;)
            {
                now = DateTime.Now;
                RefreshTimeline(now);

                try
                {
                    await Task.Delay(TimeSpan.FromSeconds(60 - now.TimeOfDay.Seconds), token);
                }
                catch (TaskCanceledException)
                {
                    break;
                }
            }
        }

        private void RefreshTimeline(DateTime now)
        {
            var startOfWeek = _week;
            var endOfWeekExclusive = _week.AddDays(7);
            int weekday;

            ((Label)_timelineLayout.Children[1]).Text = DateTime.Now.TimeOfDay.ToString(@"hh\:mm");

            if (now >= startOfWeek &&
                now < endOfWeekExclusive &&
                now.TimeOfDay >= _startTime &&
                now.TimeOfDay <= _endTime &&
                _weekdayGrids[weekday = (now - _week).Days] != null)
            {
                if (_timelineLayout.Parent != _weekdayGrids[weekday])
                {
                    if (_timelineLayout.Parent != null)
                        ((Grid)_timelineLayout.Parent).Children.Remove(_timelineLayout);

                    _weekdayGrids[weekday].Children.Add(_timelineLayout);
                }

                _timelineLayout.Margin = new Thickness(0, (now.TimeOfDay - _startTime).TotalHours * HeightPerHour, 0, 0);
                
            }
            else
            {
                if (_timelineLayout.Parent != null)
                    ((Grid)_timelineLayout.Parent).Children.Remove(_timelineLayout);
            }
        }

        private void DrawTimeLabels()
        {
            for (var time = _startTime; time <= _endTime; time = time.Add(TimeSpan.FromHours(1)))
            {
                var label = new Label
                {
                    Text = time.ToString(@"hh\:mm"),
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                };

                Grd_Timegrid.RowDefinitions.Add(new RowDefinition { Height = HeightPerHour });
                Grd_Timegrid.Children.Add(label, 0, Grd_Timegrid.RowDefinitions.Count - 1);
            }
        }

        private void DrawWeekdayLabels()
        {
            var dayOfWeek = new[]
            {
                new { Name = "Monday", Enabled = Config.Current.CurrentUser.ShowMonday },
                new { Name = "Tuesday", Enabled = Config.Current.CurrentUser.ShowTuesday },
                new { Name = "Wednesday", Enabled = Config.Current.CurrentUser.ShowWednesday },
                new { Name = "Thursday", Enabled = Config.Current.CurrentUser.ShowThursday },
                new { Name = "Friday", Enabled = Config.Current.CurrentUser.ShowFriday },
                new { Name = "Saturday", Enabled = Config.Current.CurrentUser.ShowSaturday },
                new { Name = "Sunday", Enabled = Config.Current.CurrentUser.ShowSunday },
            };

            for (int i = 0; i < 7; i++)
            {
                if (!dayOfWeek[i].Enabled)
                    continue;

                var columnDefinition = new ColumnDefinition { Width = Config.Current.DayWidth };

                Grd_Weekdays.ColumnDefinitions.Add(columnDefinition);
                Grd_Weekdays.Children.Add(_weekdayLabels[i] = new Label
                {
                    Text = dayOfWeek[i].Name,
                    HorizontalTextAlignment = TextAlignment.Center
                }, Grd_Weekdays.ColumnDefinitions.Count - 1, 0);

                Grd_Timetable.ColumnDefinitions.Add(columnDefinition);
                Grd_Timetable.Children.Add(_weekdayGrids[i] = new Grid
                {
                    BackgroundColor = (Color)App.Current.Resources["TimegridBackground"],
                    RowSpacing = 0,
                    ColumnSpacing = 0,
                    HeightRequest = Math.Floor((_endTime - _startTime).TotalHours + 1) * HeightPerHour
                }, Grd_Timetable.ColumnDefinitions.Count - 1, 0);
            }
        }

        private async Task RefreshTimetable()
        {
            var user = Config.Current.CurrentUser;

            var dayOfWeek = new[]
            {
                new { Name = "Monday", Enabled = user.ShowMonday },
                new { Name = "Tuesday", Enabled = user.ShowTuesday },
                new { Name = "Wednesday", Enabled = user.ShowWednesday },
                new { Name = "Thursday", Enabled = user.ShowThursday },
                new { Name = "Friday", Enabled = user.ShowFriday },
                new { Name = "Saturday", Enabled = user.ShowSaturday },
                new { Name = "Sunday", Enabled = user.ShowSunday },
            };

            var startOfWeek = _week;
            var endOfWeek = startOfWeek.AddDays(6);

            Period[] periods = EmptyPeriods;
            
            retry:
            try
            {
                if (_element != null)
                    periods = await Config.Current.CurrentUser.GetTimetable(_element, startOfWeek, endOfWeek);
                else
                {
                    periods = (await Config.Current.CurrentUser.GetFavoriteTimetable(startOfWeek, endOfWeek)).Where(x => user.FavoriteSubjects.Contains(x)).ToArray();
                }
            }
            catch (JsonRpcException ex) when (ex.Code == -7002) // element not found
            {
                if(await DisplayAlert("Could not find lessons",
                    "Could not find lessons.r\n\r\nDelete ALL lessons?", 
                    "Yes", 
                    "No"))
                    user.FavoriteSubjects.Clear();
                
                return;
            }
            catch (JsonRpcException ex) when ((ex.Code == -8507) || (ex.Code == -7004))// start & end in one year
            {
            }
            catch (CacheException)
            {
                await DisplayAlert("Offline cache is empty", "There is nothing in the cache for this week, disable the 'Offline Mode' in the settings.","OK");
            }
            catch (Exception ex)
            {
                if (await DisplayAlert("Failed to fetch the timetable",
                                       $"Failed to fetch the timetable from the untis server:{Environment.NewLine}{Environment.NewLine}{ex.Message}",
                                       "Retry",
                                       "Cancel"))
                {
                    goto retry;
                }
            }


            for (int i = 0; i < 7; i++)
            {
                if (!dayOfWeek[i].Enabled)
                    continue;

                var date = _week.AddDays(i);

                UpdateAndClearWeekday(dayOfWeek[i].Name, i, date);
                UpdateWeekdayGrid(dayOfWeek[i].Name, i, periods, date);
            }
        }

        private Task<Period[]> GetPeriods(DateTime startOfWeek, DateTime endOfWeek)
        {
            if (_element != null)
                return Config.Current.CurrentUser.GetTimetable(_element, startOfWeek, endOfWeek);

            return Config.Current.CurrentUser.GetFavoriteTimetable(startOfWeek, endOfWeek);
        }

        private void UpdateAndClearWeekday(string weekdayName, int weekdayIndex, DateTime date)
        {
            _weekdayLabels[weekdayIndex].Text = $"{weekdayName}{Environment.NewLine}{date:dd.MM.yyyy}";
            _weekdayGrids[weekdayIndex].Children.Clear();
            _weekdayGrids[weekdayIndex].RowDefinitions.Clear();
        }

        private void UpdateWeekdayGrid(string weekdayName, int weekdayIndex, Period[] periods, DateTime date)
        {
            var lessons = periods
                 .Where(x => x.Date == UntisDate.FromDateTime(date))
                 .GroupBy(x => x.Lsnumber)
                 .Select(x => new Lesson
                 (
                     TimeSpanEx.Max(UntisTime.ToTimeSpan(x.Min(y => y.StartTime)), _startTime),
                     TimeSpanEx.Min(UntisTime.ToTimeSpan(x.Max(y => y.EndTime)), _endTime),
                     x.ToArray()
                 ))
                 .OrderBy(x => x.StartTime)
                 .ToArray();

            DrawLessons(_weekdayGrids[weekdayIndex], lessons);
        }

        private void DrawLessons(Grid weekdayGrid, Lesson[] lessons)
        {
            var lastTime = _startTime;
            
            for (var i = 0; i < lessons.Length; i++)
            {
                var emptySpace = (lessons[i].StartTime - lastTime).TotalHours * HeightPerHour;

                if (emptySpace > 0)
                    weekdayGrid.RowDefinitions.Add(new RowDefinition { Height = emptySpace });

                var timetableView = CreateLessonOrContainerTimetableView(lessons, ref i);

                timetableView.View.Opacity = 0.9;

                var filledSpace = (timetableView.EndTime - timetableView.StartTime).TotalHours * HeightPerHour;

                if (filledSpace > 0)
                {
                    weekdayGrid.RowDefinitions.Add(new RowDefinition { Height = filledSpace });
                    weekdayGrid.Children.Add(timetableView.View, 0, weekdayGrid.RowDefinitions.Count - 1);
                }

                lastTime = timetableView.EndTime;
            }
        }

        private TimetableView CreateLessonOrContainerTimetableView(Lesson[] lessons, ref int i)
        {
            if (i + 1 >= lessons.Length || lessons[i + 1].StartTime >= lessons[i].EndTime)
            {
                return new TimetableView(startTime: lessons[i].StartTime,
                                         endTime: lessons[i].EndTime,
                                         view: CreateLessonView(lessons[i]));
            }

            return CreateContainerTimetableView(lessons, ref i);
        }

        private TimetableView CreateContainerTimetableView(Lesson[] lessons, ref int i)
        {
            var lessonStartTime = lessons[i].StartTime;
            var lessonEndTime = TimeSpanEx.Max(lessons[i].EndTime, lessons[i + 1].EndTime);

            var containerGrid = new Grid();
            var firstLessonOfGroup = i++;

            while (i + 1 < lessons.Length && lessons[i + 1].StartTime < lessonEndTime)
            {
                i++;

                if (lessons[i].EndTime > lessonEndTime)
                    lessonEndTime = lessons[i].EndTime;
            }

            var rows = lessons
                .Skip(firstLessonOfGroup)
                .Take(i - firstLessonOfGroup + 1)
                .SelectMany(x => new[] { x.StartTime, x.EndTime })
                .Distinct()
                .OrderBy(x => x)
                .ToArray();

            for (var j = 1; j < rows.Length; j++)
                containerGrid.RowDefinitions.Add(new RowDefinition { Height = (rows[j] - rows[j - 1]).TotalMinutes });

            var endTimes = new List<TimeSpan>();

            for (var j = firstLessonOfGroup; j <= i; j++)
            {
                var column = -1;

                for (var k = 0; k < endTimes.Count; k++)
                {
                    if (endTimes[k] <= lessons[j].StartTime)
                    {
                        column = k;
                        break;
                    }
                }

                if (column == -1)
                {
                    column = endTimes.Count;
                    endTimes.Add(lessons[j].EndTime);
                    containerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
                }
                else
                {
                    endTimes[column] = lessons[j].EndTime;
                }

                var rowStart = 0;

                while (rows[rowStart] != lessons[j].StartTime)
                    rowStart++;

                var rowEnd = rowStart + 1;

                while (rows[rowEnd] != lessons[j].EndTime)
                    rowEnd++;

                var lessonView = CreateLessonView(lessons[j]);

                containerGrid.Children.Add(lessonView, column, rowStart);

                if (rowEnd - rowStart > 1)
                    Grid.SetRowSpan(lessonView, rowEnd - rowStart);
            }

            var view = containerGrid;

            return new TimetableView(lessonStartTime, lessonEndTime, view);
        }

        private View CreateLessonView(Lesson lesson)
        {
            var subject = lesson.Periods.First().Su.FirstOrDefault()?.Name ?? lesson.Periods.First().Lstext ?? "UNKNOWN";
            var room = lesson.Periods.First().Ro.FirstOrDefault()?.Name ?? "UNKNOWN";
            var isFavorite = lesson.Periods.Any(x => Config.Current.CurrentUser.FavoriteSubjects.Contains(x));

            var button = new Button
            {
                Text = subject + Environment.NewLine + room,
                BindingContext = lesson,
                TextColor = (Color)App.Current.Resources["DetailPageTextColor"],
                BackgroundColor = isFavorite ? (Color)App.Current.Resources["PeriodFavoriteColor"] : (Color)App.Current.Resources["PeriodColor"]
            };
            button.Clicked += Lesson_Clicked;

            return button;
        }

        private async void DP_Date_DateSelected(object sender, DateChangedEventArgs e)
        {
            var newWeek = e.NewDate.StartOfWeek(DayOfWeek.Monday);

            if (_week != newWeek)
            {
                _week = newWeek;
                await RefreshTimetable();
                RefreshTimeline(DateTime.Now);
            }
        }


        private void Button_Today(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;

            DP_Date.Date = now;

            var day = (now.Date - _week).Days;
            var time = (now.TimeOfDay - _startTime).TotalHours;

            if (_weekdayGrids[day] != null)
            {
                SV_Timetable.ScrollToAsync(_weekdayGrids[day].X,
                                           time * HeightPerHour - Math.Abs(SV_Timetable.Height) / 2,
                                           Device.RuntimePlatform == Device.Android);
            }

        }

        private void Button_ChangeWeek(object sender, EventArgs e)
        {
            switch (((Button)sender).Text[0])
            {
                case '<':
                    DP_Date.Date = DP_Date.Date.AddDays(-7);
                    break;
                case '>':
                    DP_Date.Date = DP_Date.Date.AddDays(7);
                    break;
            }
        }

        private void Scrolled(object sender, ScrolledEventArgs e)
        {
            if (sender == SV_Weekdays)
            {
                SV_Timetable.ScrollToAsync(e.ScrollX, SV_Timetable.ScrollY, false);
            }
            else if (sender == SV_Timegrid)
            {
                SV_Timetable.ScrollToAsync(SV_Timetable.ScrollX, e.ScrollY, false);
            }
            else
            {
                SV_Weekdays.ScrollToAsync(e.ScrollX, SV_Weekdays.ScrollY, false);
                SV_Timegrid.ScrollToAsync(SV_Timegrid.ScrollX, e.ScrollY, false);
            }
        }

        private async void Lesson_Clicked(object sender, EventArgs e)
        {
            var lesson = (Lesson)((Button)sender).BindingContext;

            var period = lesson.Periods.First();
            var subject = period.Su.FirstOrDefault();
            var start = period.StartTime;
            var end = lesson.Periods.Last().EndTime;

            if (subject != null)
            {
                var isFavorite = Config.Current.CurrentUser.FavoriteSubjects.Contains(period);
                var takeAction = await DisplayAlert(subject.Description,
                                                    $"Classes:\r\n{string.Join("\r\n", period.Kl.Select(x => x.Description))}\r\n\r\n" +
                                                    $"Rooms:\r\n{string.Join("\r\n", period.Ro.Select(x => x.Description))}\r\n\r\n" +
                                                    $"Time:\r\n{start / 100}.{(start % 100) / 10}{start % 10} - {end / 100}.{(end % 100) / 10}{end % 10}\r\n\r\n" +
                                                    $"Teachers:\r\n{string.Join("\r\n", period.Te.Select(x => x.Description))}\r\n\r\n" +
                                                    $"Subjects:\r\n{string.Join("\r\n", period.Su.Select(x => x.Description))}\r\n\r\n",
                                                    isFavorite ? "Delete from my timetable" : "Add to my Timetable",
                                                    "Close");

                if (takeAction)
                {
                    if (isFavorite)
                    {
                        Config.Current.CurrentUser.FavoriteSubjects.Remove(period);
                        await DisplayAlert("Favorite deleted", $"{subject.Description} removed.", "OK");
                    }
                    else
                    {
                        Config.Current.CurrentUser.FavoriteSubjects.Add(period);
                        Config.Current.CurrentUser.InvalidateFavoriteSubjects();
                        await DisplayAlert("Favorite added", $"{subject.Description} added.", "OK");
                    }

                    // Config.Current.Save();

                    await RefreshTimetable();
                }
            }
            else
            {
                await DisplayAlert(period.Lstext,
                                   $"{period.Lstext}\r\n\r\n" +
                                   $"Rooms:\r\n{string.Join("\r\n", period.Ro.Select(x => x.Description))}\r\n\r\n" +
                                   $"Time:\r\n{start / 100}.{(start % 100) / 10}{start % 10} - {end / 100}.{(end % 100) / 10}{end % 10}\r\n\r\n",
                                   "OK");
            }
        }

        private class Lesson
        {
            public Lesson(TimeSpan startTime, TimeSpan endTime, Period[] periods)
            {
                StartTime = startTime;
                EndTime = endTime;
                Periods = periods;
            }

            public TimeSpan StartTime { get; }
            public TimeSpan EndTime { get; }
            public Period[] Periods { get; }
        }

        private class TimetableView
        {
            public TimetableView(TimeSpan startTime, TimeSpan endTime, View view)
            {
                StartTime = startTime;
                EndTime = endTime;
                View = view;
            }

            public TimeSpan StartTime { get; }
            public TimeSpan EndTime { get; }
            public View View { get; }
        }
    }
}