﻿using BetterUntis.Model;
using BetterUntis.Untis;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
        public const int TenMinutes = 10 * 60;
        public const int OneHour = 60 * 60;
        public const int OneDay = 24 * 60 * 60;

        /* 
         private bool _settingsChanged = false;
        */

		public SettingsPage ()
		{
			InitializeComponent ();

            LoadConfig();
		}

        protected override void OnDisappearing()
        {
            /*
            if (_settingsChanged)
            {
            //    Config.Current.Save();
                _settingsChanged = false;
            }
            */

    }

    private void LoadConfig()
        {
            var user = Config.Current.CurrentUser;
            // TODO

            Pckr_Theme.SelectedIndex = Config.Current.Theme;

            SC_Monday.On = user.ShowMonday;
            SC_Tuesday.On = user.ShowTuesday;
            SC_Wednesday.On = user.ShowWednesday;
            SC_Thursday.On = user.ShowThursday;
            SC_Friday.On = user.ShowFriday;
            SC_Saturday.On = user.ShowSaturday;
            SC_Sunday.On = user.ShowSunday;
            SC_GuessBuildings.On = Config.Current.GuessBuildings;

            Pckr_Cache_Policy.SelectedIndex = (int)user.CachePolicy;

            switch (Config.Current.DayWidth)
            {
                case 250:
                    Pckr_DayWidth.SelectedIndex = 0;
                    break;
                case 125:
                    Pckr_DayWidth.SelectedIndex = 1;
                    break;
                case 80:
                    Pckr_DayWidth.SelectedIndex = 2;
                    break;
            }

            switch (user.CacheTimeInSeconds)
            {
                case TenMinutes:
                    Pckr_Cache_Duration.SelectedIndex = 0;
                    break;
                case OneHour:
                    Pckr_Cache_Duration.SelectedIndex = 1;
                    break;
                case OneDay:
                    Pckr_Cache_Duration.SelectedIndex = 2;
                    break;
            }
        }

        private void Pckr_Cache_Policy_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.Current.CurrentUser.CachePolicy = (UntisCachePolicy)Pckr_Cache_Policy.SelectedIndex;
        //    _settingsChanged = true;
        }

        private void Pckr_Cache_Duration_SelectedIndexChanged(object sender, EventArgs e)
        {
            var user = Config.Current.CurrentUser;

            switch (Pckr_Cache_Duration.SelectedIndex)
            {
                case 0:
                    user.CacheTimeInSeconds = TenMinutes;
                    break;
                case 1:
                    user.CacheTimeInSeconds = OneHour;
                    break;
                case 2:
                    user.CacheTimeInSeconds = OneDay;
                    break;
            }

         //   _settingsChanged = true;
        }

        private void SwitchCell_OnChanged(object sender, ToggledEventArgs e)
        {
            var user = Config.Current.CurrentUser;
            Debug.Assert(user != null);

            var cell = (SwitchCell)sender;

            switch (cell.Text)
            {
                case "Show Mondays":
                    user.ShowMonday = cell.On;
                    break;
                case "Show Tuesdays":
                    user.ShowTuesday = cell.On;
                    break;
                case "Show Wednesdays":
                    user.ShowWednesday = cell.On;
                    break;
                case "Show Thursdays":
                    user.ShowThursday = cell.On;
                    break;
                case "Show Fridays":
                    user.ShowFriday = cell.On;
                    break;
                case "Show Saturdays":
                    user.ShowSaturday = cell.On;
                    break;
                case "Show Sundays":
                    user.ShowSunday = cell.On;
                    break;
                case "Try to guess Buildings":
                    Config.Current.GuessBuildings = cell.On;
                    break;
            }
        //    _settingsChanged = true;
        }

        private void SC_Tapped(object sender, EventArgs e)
        {
            ((SwitchCell)sender).On = !((SwitchCell)sender).On;
        }

        private void Pckr_Theme_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.Current.LoadTheme(((Picker)sender).SelectedIndex);
        //    _settingsChanged = true;
            
        }

        private void Pckr_DayWidth_SelectedIndexChange(object sender, EventArgs e)
        {
            Config.Current.setDayWidth(((Picker)sender).SelectedIndex);
        //    _settingsChanged = true;
        }

        private async void Btn_RemoveAllLessons_Clicked(object sender, EventArgs e)
        {
            if(await DisplayAlert("Delete all lessons","Are you sure you want to delete all lessons?","Yes","No"))
                Config.Current.CurrentUser.FavoriteSubjects.Clear();
        }
    }
}