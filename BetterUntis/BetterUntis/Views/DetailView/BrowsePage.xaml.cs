﻿using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BetterUntis.Untis.Models;
using BetterUntis.Model;
using System.Diagnostics;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BrowsePage : ContentPage
    {
        private UntisElement[] _elements;

        public BrowsePage()
        {
            InitializeComponent();

            Pckr_Search.SelectedIndex = 0;
            
        }

        protected override async void OnAppearing()
        {
            if (_elements != null)
                return;

            var currentUser = Config.Current.CurrentUser;

            Debug.Assert(currentUser != null);

            retry:
            try
            {
                _elements = ((UntisElement[])await currentUser.GetRooms())
                    .Concat(await currentUser.GetClasses())
                    .ToArray();
            }
            catch (Exception ex)
            {
                if (await DisplayAlert("Failed to fetch data",
                                       $"Failed to fetch data from the untis server:{Environment.NewLine}{Environment.NewLine}{ex.Message}",
                                       "Retry",
                                       "Cancel"))
                {
                    goto retry;
                }
            }

            Search_Changed(this, EventArgs.Empty);
        }

        private void Search_Changed(object sender, EventArgs e)
        {
            if (_elements == null)
                return;

            ListView_Search.BeginRefresh();

            if (String.IsNullOrWhiteSpace(Entry_Search.Text))
            {
                ListView_Search.ItemsSource = _elements.Where(DisplayElement);
            }
            else
            {
                ListView_Search.ItemsSource = _elements.Where(x =>
                    DisplayElement(x) &&
                    (x.Name.IndexOf(Entry_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                     x.LongName.IndexOf(Entry_Search.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                );
            }
            ListView_Search.EndRefresh();
        }

        private bool DisplayElement(UntisElement e)
        {
            switch (Pckr_Search.SelectedIndex)
            {
                case 0:
                    return true;
                case 1:
                    return e.Type == UntisElementType.Room;
                case 2:
                    return e.Type == UntisElementType.Class;
            }

            return false;
        }
        
        private void ListView_Search_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var associatedElement = (UntisElement)e.Item;

            Navigation.PushAsync(new TimeTablePage(associatedElement));
        }
    }
}