﻿using BetterUntis.Model;
using BetterUntis.Untis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;
using ZXing.Net.Mobile.Forms;

namespace BetterUntis.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserPage : ContentPage
    {
        public UserPage()
        {
            InitializeComponent();

            Pckr_Select_Users.ItemsSource = Config.Current.Users;

            if (Device.RuntimePlatform == Device.Windows)
                Btn_ScanQR.IsVisible = Grd_Separator.IsVisible = false;

            Pckr_AuthenticationMethod.SelectedIndex = 0;
#if DEBUG
            Entry_URL.Text = "antiope.webuntis.com";
            Entry_Schoolname.Text = "TH-Bingen";
            Entry_Username.Text = Entry_DisplayName.Text = "tuerschild";
            Pckr_AuthenticationMethod.SelectedIndex = 1;
            Entry_KeyPassword.Text = "ts1-306";
#endif
        }

        private async void Btn_Login_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (Config.Current.Users.Any(x => x.User.Authentication.Url == Entry_URL.Text &&
                                                  x.User.Authentication.School == Entry_Schoolname.Text &&
                                                  x.User.Authentication.Username == Entry_Username.Text))
                {
                    await DisplayAlert("User already exists", "The ServiceURL and username combination already exists.", "OK");
                    return;
                }

                if (Config.Current.Users.Any(x => x.DisplayName == Entry_DisplayName.Text))
                {
                    await DisplayAlert("User already exists", "The display name you chose is already taken.", "OK");
                    return;
                }
                Btn_Login.IsEnabled = false;

                var auth = new UntisAuthentication(Entry_Schoolname.Text,
                                                   Entry_URL.Text,
                                                   Entry_Username.Text,
                                                   Entry_KeyPassword.Text,
                                                   Pckr_AuthenticationMethod.SelectedIndex == 0);
                var user = new UntisUser(auth);

                await user.Login();

                var betterUser = new BetterUntisUser(user);
                betterUser.DisplayName = Entry_DisplayName.Text ?? "Unnamed";

                Config.Current.AddUser(betterUser);
                Config.Current.CurrentUser = betterUser;
             //   Config.Current.Save();

                await DisplayAlert("User Added", "You have logged in successfully.", "OK");                
            }
            catch (HttpRequestException ex)
            {
                await DisplayAlert("No internet connection", $"Verify that you have internet connection:{Environment.NewLine}{ex.Message}", "OK");
            }
            catch (JsonRpcException ex)
            {
                await DisplayAlert("Invalid credentials", $"Verify that you have entered the valid credentials:{Environment.NewLine}{ex.Message}", "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Unknown error", $"An unknown error occured:{Environment.NewLine}{ex.Message}", "OK");
            }

            Btn_Login.IsEnabled = true;
        }

        private void Entry_UserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry_DisplayName.Text = e.NewTextValue;
        }

        private void Btn_Delete_User_Clicked(object sender, EventArgs e)
        {
            if (Pckr_Select_Users.SelectedIndex >= 0)
            {
                Config.Current.DeleteUser(Pckr_Select_Users.SelectedItem as BetterUntisUser);
                Pckr_Select_Users.SelectedIndex = -1;
            }


            // ZXing_QRCode.Children.Clear();
        }

        private async void Btn_ScanQR_Clicked(object sender, EventArgs e)
        {
            var options = new ZXing.Mobile.MobileBarcodeScanningOptions
            {
                AutoRotate = false,
                TryHarder = true,
                PossibleFormats = new List<BarcodeFormat>
                {
                   BarcodeFormat.QR_CODE
                }
            };

            var scanPage = new ZXingScannerPage(options);

            scanPage.OnScanResult += (result) =>
            {
                scanPage.IsScanning = false;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopAsync();
                    LoginWithQRCode(result.Text);
                });
            };

            await Navigation.PushAsync(scanPage);
        }

        private void LoginWithQRCode(string text)
        {
            if (text.StartsWith("BetterUntis:"))
            {
                text = text.Substring("BetterUntis:".Length);
                var buui = JsonConvert.DeserializeObject<UntisAuthentication>(text);

                Entry_URL.Text = buui.Url;
                Entry_Schoolname.Text = buui.School;
                Entry_Username.Text = Entry_DisplayName.Text = buui.Username;
                Pckr_AuthenticationMethod.SelectedIndex = buui.PasswordIsKey ? 0 : 1;
                Entry_KeyPassword.Text = buui.Password;


                return;
            }

            var regex = new Regex("untis:\\/\\/setschool\\?url=([^&]+)&school=([^&]+)&user=([^&]+)&key=([^&]+)");

            var match = regex.Match(text);

            if (!match.Success)
            {
                DisplayAlert("Wrong QR-Code", "You have scanned a wrong QR-Code.", "OK");
                return;
            }

            Entry_URL.Text = match.Groups[1].Value;
            Entry_Schoolname.Text = match.Groups[2].Value;
            Entry_Username.Text = Entry_DisplayName.Text = match.Groups[3].Value;
            Pckr_AuthenticationMethod.SelectedIndex = 0;
            Entry_KeyPassword.Text = match.Groups[4].Value;
        }

        private void Swtch_ShowKeyPassword_Toggled(object sender, ToggledEventArgs e)
        {
            Entry_KeyPassword.IsPassword = !e.Value;
        }

        private void Pckr_Select_Users_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var user = (BetterUntisUser)Pckr_Select_Users.SelectedItem;

                if (user == null)
                    return;

                Entry_URL.Text = user.User.Authentication.Url;
                Entry_Schoolname.Text = user.User.Authentication.School;
                Entry_Username.Text = user.User.Authentication.Username;
                Entry_DisplayName.Text = user.DisplayName;

                Pckr_AuthenticationMethod.SelectedIndex = user.User.Authentication.PasswordIsKey ? 0 : 1;
                Entry_KeyPassword.Text = user.User.Authentication.Password;

                /*
                if (Device.RuntimePlatform != Device.Windows)
                    return;

                ZXing_QRCode.Children.Clear();
                
                var userQr = new ZXingBarcodeImageView()
                {
                    BarcodeFormat = BarcodeFormat.QR_CODE,
                    BarcodeValue = "BetterUntis:" + JsonConvert.SerializeObject(user.User.Authentication),
                    HorizontalOptions = LayoutOptions.Center,
                    BarcodeOptions = new ZXing.Common.EncodingOptions
                    {
                        Width = 200,
                        Height = 200,
                        Margin = 0,
                        PureBarcode = true
                    }

                };

                ZXing_QRCode.Children.Add(new Label
                {
                    Text = "User QR Code:",
                    HorizontalTextAlignment = TextAlignment.Center,
                    TextColor = (Color)App.Current.Resources["DetailPageTextColor"]
                });
                ZXing_QRCode.Children.Add(userQr);
                */
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }

        private void Pckr_AuthenticationMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((Picker)sender).SelectedIndex)
            {
                case 0:
                    Lbl_KeyPassword.Text = "API key:";
                    Entry_KeyPassword.Placeholder = "API key";
                    Lbl_ShowKeyPassword.Text = "Show API key:";
                    break;
                case 1:
                    Lbl_KeyPassword.Text = "Password:";
                    Entry_KeyPassword.Placeholder = "Password";
                    Lbl_ShowKeyPassword.Text = "Show password:";
                    break;
            }
        }
    }
}