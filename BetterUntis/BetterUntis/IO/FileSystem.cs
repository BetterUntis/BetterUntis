﻿using WorkingWithFiles;
using Xamarin.Forms;

namespace BetterUntis.IO
{
    public static class FileSystem
    {
        public static void SaveFile(string filename, string txt)
        {
            DependencyService.Get<ISaveAndLoad>().Save(filename, txt);
        }

        public static string LoadFile(string filename)
        {
            return DependencyService.Get<ISaveAndLoad>().Load(filename);
        }
    }
}
