﻿using System;

namespace BetterUntis
{
    public class JsonRpcException : Exception
    {
        public const int NotAuthenticated = -8520;
        public const int ParseError = -32700;

		public JsonRpcException(int code, string message) : base(message)
        {
            Code = code;
        }

        public int Code { get; }

		public static JsonRpcException FromParseError(string message)
		{
			return new JsonRpcException(ParseError, message);
		}
    }
}
