﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;

namespace BetterUntis.JsonRpc
{
    public static class JsonRpc
    {
        private const string JsonRpcVersion = "2.0";
        
        public static StringContent CreateRequestContent(string id, string method, JToken parameters)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            var requestObject = CreateRequestObject(id, method, parameters);

            return new StringContent(requestObject.ToString(),
                                     Encoding.UTF8,
                                     "application/json");
        }

        public static JToken ParseResponseContent(string content)
        {
            var responseObject = JObject.Parse(content);

            if (responseObject.TryGetValue("error", out var error))
            {
                var code = error["code"]?.ToObject<int>() ?? -1;
                var message = error["message"]?.ToObject<string>() ?? "Unknown error.";

                throw new JsonRpcException(code, message);
            }

            var result = responseObject["result"];

            if (result == null)
                throw JsonRpcException.FromParseError("Invalid JSON RPC response.");

            return result;
        }

        private static JObject CreateRequestObject(string id, string method, JToken parameters)
        {
            var request = new JObject();

            if (id != null)
                request.Add("id", id);

            request.Add("method", method);

            if (parameters != null)
                request.Add("params", parameters);

            request.Add("jsonrpc", JsonRpcVersion);

            return request;
        }
    }
}
