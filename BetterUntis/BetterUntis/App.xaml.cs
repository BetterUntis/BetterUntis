﻿using BetterUntis.Model;
using BetterUntis.Untis.Models;
using BetterUntis.Views.Menu;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace BetterUntis
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            
            Config.Load();
            Config.Current.LoadTheme();

            MainPage = new MasterDetail();
        }

        protected override void OnStart()
        {            
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Config.Current.Save();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
