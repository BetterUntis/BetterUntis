﻿using System;

namespace BetterUntis.Extensions
{
    public class ConvertEx
    {
        public static byte[] FromBase32String(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            input = input.TrimEnd('='); //remove padding characters
            int byteCount = input.Length * 5 / 8; //this must be TRUNCATED
            byte[] returnArray = new byte[byteCount];

            byte curByte = 0, bitsRemaining = 8;
            int mask = 0, arrayIndex = 0;

            foreach (char c in input)
            {
                int cValue = CharToValue(c);

                if (bitsRemaining > 5)
                {
                    mask = cValue << (bitsRemaining - 5);
                    curByte = (byte)(curByte | mask);
                    bitsRemaining -= 5;
                }
                else
                {
                    mask = cValue >> (5 - bitsRemaining);
                    curByte = (byte)(curByte | mask);
                    returnArray[arrayIndex++] = curByte;
                    curByte = (byte)(cValue << (3 + bitsRemaining));
                    bitsRemaining += 3;
                }
            }

            //if we didn't end with a full byte
            if (arrayIndex != byteCount)
            {
                returnArray[arrayIndex] = curByte;
            }

            return returnArray;
        }

        private static int CharToValue(char c)
        {
            const int numberOfLetters = ('Z' - 'A' + 1);

            var value = (int)c;

            if (value >= 'A' && value <= 'Z')
                return value - 'A';

            if (value >= 'a' && value <= 'z')
                return value - 'a';

            if (value >= '2' && value <= '7')
                return value - '2' + numberOfLetters;

            throw new ArgumentException("Invalid Base32 character.", nameof(c));
        }
    }
}