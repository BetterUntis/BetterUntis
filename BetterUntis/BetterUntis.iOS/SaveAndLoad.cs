﻿using System;

using WorkingWithFiles;
using Xamarin.Forms;
using System.IO;
using BetterUntis.IO;

[assembly: Dependency(typeof(SaveAndLoad))]
namespace BetterUntis.IO
{
    public class SaveAndLoad : ISaveAndLoad
    {
        public void Save(string fileName, string text)
        {
            File.WriteAllText(GetPath(fileName), text);
        }

        public string Load(string fileName)
        {
            return File.ReadAllText(GetPath(fileName));
        }

        private static string GetPath(string fileName)
        {
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(directory, fileName);

            return path;
        }
    }
}