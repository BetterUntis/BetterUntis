﻿using System;
using Windows.Storage;
using WorkingWithFiles;
using Xamarin.Forms;

[assembly: Dependency(typeof(SaveAndLoad_WinApp))]
namespace WorkingWithFiles
{
    // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/hh758325.aspx
    public class SaveAndLoad_WinApp : ISaveAndLoad
    {
        public void Save(string fileName, string text)
        {
            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = localFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting).AsTask().Result;
            FileIO.WriteTextAsync(sampleFile, text).AsTask().Wait();
        }

        public string Load(string fileName)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = storageFolder.GetFileAsync(fileName).AsTask().Result;
            return FileIO.ReadTextAsync(sampleFile).AsTask().Result;
        }
    }
}