﻿namespace BetterUntis.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new BetterUntis.App());
        }
    }
}
