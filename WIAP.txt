Project: BetterUntis

Studenten:
    Maximilian Conradi 199959
    Stefan Zander 199881
    Sebastian Fuchs 199939

Umsetzung mittels:
	C# und Xamerin Forms in Visual Studio 2017

Was soll es können:
	studiengangauflistung
	Raumauflistung
	Stundenplan zusammenstellen
	server / json basiert (account auf basis von webuntis daten)
	offline cache
	2 von 3 devices

Optionale Funktionen:
	QR-scanner
	farben pro fach auswählen kann
	sprachen
	freie raumsuche
	Dozentenaufstellung/dozentensuche
	azure sync

Zeitrahmen:
    ab 08.06
        GUI
            Login
            Studiengangauflistung
            Raumauflistung
            Stundenplan
            Einstellungen
        API
            WebUntis async API mit rpc
            
            
    ab 29.06
        - background service mit server
        - synchronization
        - Implementierung von Einstellungen
            
    ab 13.07
        - optionale features
            
    geplante deadline: 27.07